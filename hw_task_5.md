1. Выбрать оптимальное количество главных компонент в датасете MNIST. Показать график, на основе которого было принято решение.
2. На основе имеющегося кода перспептрона построить классификатор для датасета пониженной размерности.
3. Обучить его, сравнить показатели качества работы с обученным ранее персептроном для исходного датасета.
4. Сделать вывод о том, как повлияло понижение размерности с помощью PCA на качество классификации.



#1
Results:
Accuracy without pca: 0.8849999904632568
Accuracy with pca: 0.8769999742507935


#2
Results:
Accuracy without pca: 0.8849999904632568
Accuracy with pca: 0.875


#3
Results:
Accuracy without pca: 0.8830000162124634
Accuracy with pca: 0.878000020980835


#4
Results:
Accuracy without pca: 0.8799999952316284
Accuracy with pca: 0.8799999952316284


#5
Results:
Accuracy without pca: 0.8880000114440918
Accuracy with pca: 0.8790000081062317




pca = PCA(10)

Results:
Accuracy without pca: 0.8859999775886536
Accuracy with pca: 0.7760000228881836