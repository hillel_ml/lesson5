import keras 
from  keras.datasets import mnist
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


# Imports from optuna_nn_test
from pickle import FALSE
import optuna
import torch
import torch.nn as nn
import torch.optim as optim
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from pprint import pformat

# Imports from tf_board
import datetime
import tensorflow
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.utils import to_categorical


# Additioanl imports
from keras.backend import clear_session
from keras.layers import Conv2D
from keras.layers import Flatten
from tensorflow.keras.optimizers import RMSprop


N_TRAIN_EXAMPLES = 3000
N_VALID_EXAMPLES = 1000
BATCHSIZE = 128
CLASSES = 10
EPOCHS = 10

# Configuration options
feature_vector_length = 784
num_classes = 10
fin_results_wo_pca = {}
fin_results_w_pca = {}
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tensorflow.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)


def objective(trial):

    # Clear clutter from previous Keras session graphs.
    clear_session()
  
    model = Sequential()
    model.add(
        Conv2D(
            filters=trial.suggest_categorical("filters", [32, 64]),
            kernel_size=trial.suggest_categorical("kernel_size", [3, 5]),
            strides=trial.suggest_categorical("strides", [1, 2]),
            activation=trial.suggest_categorical("activation", ['relu',
                                                                'softmax',
                                                                'softplus',
                                                                'softsign',
                                                                'gelu',
                                                                'sigmoid',
                                                                'tanh',
                                                                'selu',
                                                                'linear'
                                                                ]),
            input_shape=input_shape,
        )
    )
    model.add(Flatten())
    model.add(Dense(CLASSES, activation="softmax"))
    # model.add(Dense(350, input_shape=input_shape, activation='relu'))
    # model.add(Dense(50, activation='relu'))
    # model.add(Dropout(0.5))

    # We compile our model with a sampled learning rate.
    learning_rate = trial.suggest_float("learning_rate", 1e-5, 1e-1, log=True)
    dropout = trial.suggest_float("dropout", 0.1, 1.0)
    optimizer = trial.suggest_categorical("optimizer", ["Adadelta",
                                                        "Adagrad",
                                                        "Adam",
                                                        "Adamax",
                                                        "NAdam",
                                                        "RMSprop",
                                                        "SGD"
                                                        ])
    n_unit = trial.suggest_int("n_unit", 4, 18)
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=RMSprop(learning_rate=learning_rate),
        metrics=["accuracy"],
    )

    model.fit(
        x_train,
        y_train,
        validation_data=(x_valid, y_valid),
        shuffle=True,
        batch_size=BATCHSIZE,
        epochs=EPOCHS,
        verbose=False,
        validation_split=0.2,
        callbacks=[tensorboard_callback]
    )

    # Evaluate the model accuracy on the validation set.
    score = model.evaluate(x_valid, y_valid, verbose=0)
    print(f'Test results - Loss: {score[0]} - Accuracy: {score[1]}%')
    return score[1]


def objective_w (trial):

    # Clear clutter from previous Keras session graphs.
    clear_session()
  
    model = Sequential()
    model.add(
        Conv2D(
            filters=trial.suggest_categorical("filters", [32, 64]),
            kernel_size=trial.suggest_categorical("kernel_size", [3, 5]),
            strides=trial.suggest_categorical("strides", [1, 2]),
            activation=trial.suggest_categorical("activation", ['relu',
                                                                'softmax',
                                                                'softplus',
                                                                'softsign',
                                                                'gelu',
                                                                'sigmoid',
                                                                'tanh',
                                                                'selu',
                                                                'linear'
                                                                ]),
            input_shape=input_shape,
        )
    )
    model.add(Flatten())
    model.add(Dense(CLASSES, activation="softmax"))
    # model.add(Dense(350, input_shape=input_shape, activation='relu'))
    # model.add(Dense(50, activation='relu'))
    # model.add(Dropout(0.5))

    # We compile our model with a sampled learning rate.
    learning_rate = trial.suggest_float("learning_rate", 1e-5, 1e-1, log=True)
    dropout = trial.suggest_float("dropout", 0.1, 1.0)
    optimizer = trial.suggest_categorical("optimizer", ["Adadelta",
                                                        "Adagrad",
                                                        "Adam",
                                                        "Adamax",
                                                        "NAdam",
                                                        "RMSprop",
                                                        "SGD"
                                                        ])
    n_unit = trial.suggest_int("n_unit", 4, 18)
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=RMSprop(learning_rate=learning_rate),
        metrics=["accuracy"],
    )

    model.fit(
        approximation_train,
        y_train,
        validation_data=(approximation_valid, y_valid),
        shuffle=True,
        batch_size=BATCHSIZE,
        epochs=EPOCHS,
        verbose=False,
        validation_split=0.2,
        callbacks=[tensorboard_callback]
    )

    # Evaluate the model accuracy on the validation set.
    score = model.evaluate(x_valid, y_valid, verbose=0)
    print(f'Test results - Loss: {score[0]} - Accuracy: {score[1]}%')
    return score[1]




(x_train, y_train), (x_valid, y_valid) = mnist.load_data()

img_x, img_y = x_train.shape[1], x_train.shape[2]

x_train = np.reshape(x_train,(60000,784))
x_valid = np.reshape(x_valid,(10000,784))
x_train = x_train/255
x_valid = x_valid/255


print("Shape of Train Data: {}".format(x_train.shape))
print("Shape of Test Data: {}".format(x_valid.shape))


# выбор количества компонент
pca = PCA().fit(x_train.data)
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance')
plt.grid(True)
plt.show()


pca = PCA(32)
pca.fit(x_train)
x_train_pca = pca.transform(x_train)
x_valid_pca = pca.transform(x_valid)
approximation_train = pca.inverse_transform(x_train_pca)
approximation_valid = pca.inverse_transform(x_valid_pca)


approximation_train = approximation_train.reshape(-1, img_x, img_y, 1)[:N_TRAIN_EXAMPLES].astype("float32") / 255
approximation_valid = approximation_valid.reshape(-1, img_x, img_y, 1)[:N_VALID_EXAMPLES].astype("float32") / 255


x_train = x_train.reshape(-1, img_x, img_y, 1)[:N_TRAIN_EXAMPLES].astype("float32") / 255
x_valid = x_valid.reshape(-1, img_x, img_y, 1)[:N_VALID_EXAMPLES].astype("float32") / 255
y_train = y_train[:N_TRAIN_EXAMPLES]
y_valid = y_valid[:N_VALID_EXAMPLES]


# print("Shape of Train Data: {}".format(x_train.shape))
# print("Shape of Test Data: {}".format(x_valid.shape))



input_shape = (img_x, img_y, 1)



study_wo_pca = optuna.create_study(direction="maximize", sampler=optuna.samplers.TPESampler(), pruner=optuna.pruners.MedianPruner())
study_wo_pca.optimize(objective, n_trials=100, timeout=600)

best_trial_wo_pca = study_wo_pca.best_trial

for key, value in best_trial_wo_pca.params.items():
    fin_results_wo_pca[key] = value




study_w_pca = optuna.create_study(direction="maximize", sampler=optuna.samplers.TPESampler(), pruner=optuna.pruners.MedianPruner())
study_w_pca.optimize(objective_w, n_trials=100, timeout=600)

best_trial_w_pca = study_w_pca.best_trial

for key, value in best_trial_w_pca.params.items():
    fin_results_w_pca[key] = value




print("Results:")
print("Accuracy without pca: {}".format(best_trial_wo_pca.value))
print("Accuracy with pca: {}".format(best_trial_w_pca.value))





# print("Number of finished trials: {}".format(len(study_wo_pca.trials)))
# print("Best trial number: {}".format(best_trial_wo_pca.number))
# print("Accuracy: {}".format(best_trial_wo_pca.value))
# print("Dropout: {}".format(fin_results_wo_pca['dropout']))
# print("Activation function: {}".format(fin_results_wo_pca['activation']))
# print("Optimization algorithm: {}".format(fin_results_wo_pca['optimizer']))


# (X_TRAIN,Y_TRAIN),(X_TEST,Y_TEST) = mnist.load_data()


# print("approximation_TRAIN: {}".format(approximation_TRAIN))
# print("approximation_TEST: {}".format(approximation_TEST))