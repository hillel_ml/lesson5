import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

#Процедура для рисования вектора
def draw_vector(v0, v1, ax=None):
    ax = ax or plt.gca()
    arrowprops = dict(arrowstyle='->',
                      linewidth=2,
                      shrinkA=0,
                      shrinkB=0)

    ax.annotate('', v1, v0, arrowprops=arrowprops)

# Создание набора случайных величин
rng = np.random.RandomState(1)
X = np.dot(rng.rand(2, 2), rng.randn(2, 200)).T

# Определение главных компонент
pca = PCA(n_components=2, whiten=True)
pca.fit(X)

# Компоненты
components = pca.components_
print(components)

# Объяснимая дисперсия
variance = pca.explained_variance_
print(variance)

# Прорисовка данных и векторов главных компонент
plt.scatter(X[:, 0], X[:, 1], alpha=0.2)

for length, vector in zip(variance, components):
    v = vector * 3 * np.sqrt(length)
    draw_vector(pca.mean_, pca.mean_ + v)

plt.axis('equal')
plt.grid(True)
plt.show()

# Прорисовка в проекции на оси главных компонент
X_pca = pca.transform(X)
plt.scatter(X_pca[:, 0], X_pca[:, 1], alpha=0.2)
draw_vector([0, 0], [0, 3])
draw_vector([0, 0], [3, 0])
plt.axis('equal')
plt.xlabel('component 1')
plt.ylabel('component 2')
plt.title('principal components')
plt.grid(True)
plt.show()

# Понижение размерности
pca = PCA(n_components=1)
pca.fit(X)
X_pca = pca.transform(X)
print('original shape    : ', X.shape)
print('transformed shape : ', X_pca.shape)

X_new = pca.inverse_transform(X_pca)
plt.scatter(X[:, 0], X[:, 1], c='r', alpha=0.8)
plt.scatter(X_new[:, 0], X_new[:, 1], c='g', alpha=0.4)
plt.axis('equal')
plt.grid(True)
plt.show()