import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
#import seaborn as sns

from sklearn.datasets import load_digits
digits = load_digits()

pca = PCA(2)  # понижение размерности с 64 до 2 измерений
projected = pca.fit_transform(digits.data)
print('original shape    : ', digits.data.shape)
print('transformed shape : ', projected.shape)

plt.scatter(projected[:, 0],
            projected[:, 1],
            c=digits.target,
            edgecolor='none',
            alpha=0.5,
            cmap=plt.cm.get_cmap('Spectral', 10))
plt.xlabel('component 1')
plt.ylabel('component 2')
plt.colorbar()

plt.grid(True)
plt.show()

# Процедура для отображения базиса
def plot_pca_components(x, coefficients=None, mean=0, components=None,
                        imshape=(8, 8), n_components=8, fontsize=12,
                        show_mean=True):
    if coefficients is None:
        coefficients = x

    if components is None:
        components = np.eye(len(coefficients), len(x))

    mean = np.zeros_like(x) + mean

    fig = plt.figure(figsize=(1.2 * (5 + n_components), 1.2 * 2))
    g = plt.GridSpec(2, 4 + bool(show_mean) + n_components, hspace=0.3)

    def show(i, j, x, title=None):
        ax = fig.add_subplot(g[i, j], xticks=[], yticks=[])
        ax.imshow(x.reshape(imshape), interpolation='nearest')
        if title:
            ax.set_title(title, fontsize=fontsize)

    show(slice(2), slice(2), x, "True")

    approx = mean.copy()

    counter = 2
    if show_mean:
        show(0, 2, np.zeros_like(x) + mean, r'$\mu$')
        show(1, 2, approx, r'$1 \cdot \mu$')
        counter += 1

    for i in range(n_components):
        approx = approx + coefficients[i] * components[i]
        show(0, i + counter, components[i], r'$c_{0}$'.format(i + 1))
        show(1, i + counter, approx,
             r"${0:.2f} \cdot c_{1}$".format(coefficients[i], i + 1))
        if show_mean or i > 0:
            plt.gca().text(0, 1.05, '$+$', ha='right', va='bottom',
                           transform=plt.gca().transAxes, fontsize=fontsize)

    show(slice(2), slice(-2, None), approx, "Approx")
    return fig

# Отображение в базисе 8 пикселей
fig = plot_pca_components(digits.data[10],
                          show_mean=False)

plt.show(fig)

# Отображение в базисе 8 главных компонент
pca = PCA(n_components=8)
Xproj = pca.fit_transform(digits.data)
fig = plot_pca_components(digits.data[10], Xproj[10],
                          pca.mean_, pca.components_)
plt.show(fig)

# выбор количества компонент
pca = PCA().fit(digits.data)
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance')
plt.grid(True)
plt.show()

# фильтрация шума
# незашумленные данные
def plot_digits(data):
    fig, axes = plt.subplots(4, 10, figsize=(10, 4),
                             subplot_kw={'xticks':[], 'yticks':[]},
                             gridspec_kw=dict(hspace=0.1, wspace=0.1))
    for i, ax in enumerate(axes.flat):
        ax.imshow(data[i].reshape(8, 8),
                  cmap='binary', interpolation='nearest',
                  clim=(0, 16))

    return fig

plt.show(plot_digits(digits.data))
# зашумленные данные
np.random.seed(42)
noisy = np.random.normal(digits.data, 4)

plt.show(plot_digits(noisy))
pca = PCA(0.50).fit(noisy)
print(pca.n_components_)

# восстановление данных
components = pca.transform(noisy)
filtered = pca.inverse_transform(components)
plt.show(plot_digits(filtered))

